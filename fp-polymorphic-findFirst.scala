def findFirst[A](as: Array[A], p: A => Boolean): Int = {
  @annotation.tailrec
  def go(n: Int): Int = 
    if (n >= as.length) -1
    else if (p(as(n))) n 
    else go(n + 1)

  go(0)
}

val cats = Array("misty", "toby", "mitzi")
val answers = Array(200, 13, 45, 87, 42, 99)

println( findFirst[String](cats, (cat: String) => cat == "mitzi") )
println( findFirst[Int](answers, (ans: Int) => ans == 42) )