val u_stuff = Seq("Apples", "Oranges", "Bananas", "Grapes", "Mangos")
val s_stuff = u_stuff.sortWith(_.compareTo(_) < 0)

def compareAsc(a: String, b: String) = a < b

def isSorted[A](things: Seq[A], compare: (A, A) => Boolean) = {
  def go(s: Seq[A], i: Int): Boolean = {
    if((s.length -1) < (i + 1)) return true
    
    // println(s"${s(i)} ${s(i + 1)} ${s(i) < s(i + 1)}")
    if (compare(s(i), s(i + 1))) go(s, i + 1) else false
  }
  go(things, 0)
}

println(s"$u_stuff sorted? ${isSorted(u_stuff, compareAsc)}")
println("----------------")
println(s"$s_stuff sorted? ${isSorted(s_stuff, compareAsc)}")