
import math.abs

def fibonacci(p: Int, c: Int): Unit = {
  def go(p: Int, c: Int): Unit = {
    if (c > 1000) return
    println(c)
    
    c match {
      case 0 => go(c, 1)
      case nc => go(nc, nc + p)
    }
   
  }
  go(p, c)
}

fibonacci(0,0)