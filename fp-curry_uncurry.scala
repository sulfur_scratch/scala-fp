// exercise 2.3
def curry[A,B,C](f: (A,B) => C): A => (B => C) =
  (a: A) => (b: B) => f(a,b) 

val cf = curry((a: String,b: String) => { a + b })
println(s"curry'ing ${cf("aaa")("bbb")}")



// exercise 2.4
def uncurry[A,B,C](f: A => B => C): (A,B) => C =
  (a: A, b: B) => f(a)(b)

val ucf = uncurry(cf)
println(s"uncurry'ing ${ucf("ccc", "ddd")}")