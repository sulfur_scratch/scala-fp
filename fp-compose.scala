import math._

// exercise 2.5
def compose[A,B,C](f: B => C, g: A => B): A => C = 
  (a: A) => f(g(a))

val square = (m: Int) => m * m
val add4 = (m: Int) => m + 4

println(s"${compose(square, add4)(2)}")
println(s"${compose(add4, square)(2)}")

// native compose functionality...
val cos = ((x: Double) => Pi / 2 - x ) andThen math.sin
println(s"cos'ing ${cos(10)}")